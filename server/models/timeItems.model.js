import bookshelf from '../config/bookshelf';

/**
 * User model.
 */
class TimeItems extends bookshelf.Model {
    get tableName() {
        return 'time_items';
    }

    get hasTimestamps() {
        return false;
    }

}

export default TimeItems;