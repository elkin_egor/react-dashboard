import express from 'express';
import authRoutes from './auth.route';
import userRoutes from './user.route';
import timeItemsRoutes from './timeItems.route';

const router = express.Router();

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount user routes at /users
router.use('/users', userRoutes);

// mount user routes at /users
router.use('/time_items', timeItemsRoutes);

export default router;