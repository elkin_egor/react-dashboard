import bcrypt from 'bcrypt';
import HttpStatus from 'http-status-codes';
import TimeItems from '../models/timeItems.model';

/**
 * Find all the timeItems
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function findAll(req, res) {
    TimeItems.forge()
        .fetchAll()
        .then(timeItems => res.json({
                error: false,
                datatype: 'time_items',
                data: timeItems.toJSON()
            })
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
} 

/**
 *  Find timeItem by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function findById(req, res) {
    TimeItems.forge({id: req.params.id})
        .fetch()
        .then(timeItem => {
            if (!timeItem) {
                res.status(HttpStatus.NOT_FOUND).json({
                    error: true, data: {}
                });
            }
            else {
                res.json({
                    error: false,
                    data: timeItem.toJSON()
                });
            }
        })
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}

/**
 * Store new timeItem
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function store(req, res) {
    const {time_spent, description, category_id, project_id, user_id, item_date} = req.body;

    const work_date = (new Date(item_date).getTime()/1000);

    TimeItems.forge({
        user_id, work_date, project_id, category_id, time_spent, description
    }).save()
        .then(timeItem => res.json({
                success: true,
                data: timeItem.toJSON()
            })
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}

/**
 * Update product by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
/*
export function update(req, res) {
    TimeItems.forge({id: req.params.id})
        .fetch({require: true})
        .then(product => product.save({
                first_name: req.body.first_name || product.get('first_name'),
                last_name: req.body.last_name || product.get('last_name'),
                email: req.body.email || product.get('email')
            })
                .then(() => res.json({
                        error: false,
                        data: product.toJSON()
                    })
                )
                .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                        error: true,
                        data: {message: err.message}
                    })
                )
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}*/

/**
 * Destroy product by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
 /*
export function destroy(req, res) {
    TimeItems.forge({id: req.params.id})
        .fetch({require: true})
        .then(product => product.destroy()
            .then(() => res.json({
                    error: false,
                    data: {message: 'TimeItems deleted successfully.'}
                })
            )
            .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    error: true,
                    data: {message: err.message}
                })
            )
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}*/