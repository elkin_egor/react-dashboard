import bcrypt from 'bcrypt';
import HttpStatus from 'http-status-codes';
import User from '../models/user.model';
import jwt from 'jsonwebtoken';

/**
 * Find all the users
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function findAll(req, res) {
    User.forge()
        .fetchAll()
        .then(user => res.json({
                error: false,
                data: user.toJSON()
            })
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}

/**
 *  Find user by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function findById(req, res) {
    User.forge({id: req.params.id})
        .fetch()
        .then(user => {
            if (!user) {
                res.status(HttpStatus.NOT_FOUND).json({
                    error: true, data: {}
                });
            }
            else {
                res.json({
                    error: false,
                    data: user.toJSON()
                });
            }
        })
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}

/**
 * Store new user
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function store(req, res) {
    const {first_name, last_name, email} = req.body;
    console.log('server store');
    console.log(first_name);
    const password = bcrypt.hashSync(req.body.password, 10);

    User.query({
        where: {email: email},
    }).fetch().then(user => {
        if (user) {
            res.status(HttpStatus.UNAUTHORIZED).json({
                error: true, message: 'User already exist!'
            });
        } else {
            User.forge({
                first_name, last_name, email, password
            }, {hasTimestamps: true})
            .save()
            .then(user => {

                console.log('then');
                if (user) {
                    console.log('user');
                    console.log(user);
                    console.log(user.get('password'));


                    const token = jwt.sign({
                        id: user.get('id'),
                        email: user.get('email')
                    }, process.env.TOKEN_SECRET_KEY);

                    res.json({
                        token,
                        email:  user.get('email'),
                        fname:  user.get('first_name'),
                        lname:  user.get('last_name')
                    });

                } else {

                    console.log('333');

                    logger.log('error', 'Invalid username or password.');

                    res.status(HttpStatus.UNAUTHORIZED).json({
                        error: true, message: 'Invalid username or password.'
                    });
                }
            })
            .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    error: err
                })
            );
        }
    });
}

/**
 * Update user by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function update(req, res) {
    User.forge({id: req.params.id})
        .fetch({require: true})
        .then(user => user.save({
                first_name: req.body.first_name || user.get('first_name'),
                last_name: req.body.last_name || user.get('last_name'),
                email: req.body.email || user.get('email')
            })
                .then(() => res.json({
                        error: false,
                        data: user.toJSON()
                    })
                )
                .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                        error: true,
                        data: {message: err.message}
                    })
                )
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}

/**
 * Destroy user by id
 *
 * @param {object} req
 * @param {object} res
 * @returns {*}
 */
export function destroy(req, res) {
    User.forge({id: req.params.id})
        .fetch({require: true})
        .then(user => user.destroy()
            .then(() => res.json({
                    error: false,
                    data: {message: 'User deleted successfully.'}
                })
            )
            .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                    error: true,
                    data: {message: err.message}
                })
            )
        )
        .catch(err => res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
                error: err
            })
        );
}