import React from 'react';
 
// Import routing components
import {Route, Switch} from 'react-router-dom';
import {ConnectedRouter} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory'
const history = createHistory();

// Import custom components
import MainLayout from '../components/common/layout/MainLayout';
import NotFound from '../components/error/NotFound';
import LoginContainer from '../containers/auth/LoginContainer';
import SignUpContainer from '../containers/auth/SignUpContainer';
import DashboardContainer from '../containers/dashboard/DashboardContainer';
import AuthenticatedRoute from './AuthenticatedRoute';
import NotAuthRoute from './NotAuthRoute';


const Router = () => (
    <ConnectedRouter history={history}>
        <Switch>
            <NotAuthRoute exact path="/" component={LoginContainer}/>
            <NotAuthRoute path="/signup" component={SignUpContainer}/>

            <MainLayout>
                <Switch>
                    <AuthenticatedRoute path="/dashboard" component={DashboardContainer}/>
                </Switch>
            </MainLayout>

            <Route component={NotFound}/>
        </Switch>
    </ConnectedRouter>
);

export default Router;
