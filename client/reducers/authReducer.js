// Import custom components
import {
    LOG_IN_SUCCESS, 
    LOG_IN_FAILURE, 
    LOG_OUT_SUCCESS, 
    REMOVE_LOGIN_ERROR,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAILURE,
    REMOVE_SIGN_UP_ERROR
} from '../constants/actionType';

var initialState = {
    token: null,
    isAuthenticated: false,
    email: null,
    fname:null,
    lname:null,
    loginError: false,
    signUpError: false,
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
export default function (state, action) {
    state = state || initialState;

    console.log('authReducer');
    console.log(action);
    console.log(state);

    switch (action.type) {
        case LOG_IN_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: true,
                token: action.data.token,
                email: action.data.email,
                fname: action.data.fname,
                lname: action.data.lname,
                loginError: false,
                signUpError: false
            });

        case LOG_IN_FAILURE:
            return Object.assign({}, state, {
                isAuthenticated: false,
                token: null,
                loginError: true
            });

        case REMOVE_LOGIN_ERROR:
            return Object.assign({}, state, {
                loginError: false,
            });

        case LOG_OUT_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: false,
                token: null,
                email: null,
                fname: null,
                lname: null,
            });

        case SIGN_UP_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: true,
                token: action.data.token,
                email: action.data.email,
                fname: action.data.fname,
                lname: action.data.lname,
                loginError: false,
                signUpError: false,
            });

        case SIGN_UP_FAILURE:
            return Object.assign({}, state, {
                signUpError: true
            });

        case REMOVE_SIGN_UP_ERROR:
            return Object.assign({}, state, {
                signUpError: false
            });

        default:
            return state;
    }
}