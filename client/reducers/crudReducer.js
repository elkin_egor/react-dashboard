import {
    ENTITY_CREATE,
    ENTITY_UPDATE,
    ENTITY_FETCH,
    SELECT_ENTITY_ITEM,
    ENTITY_DELETE,
    CLEAR_ENTITY_LIST,
    GET_ENTITY_LIST,
} from '../constants/actionType';


let initialState = {
    timeItems: [],
    selectedItem: {
        product: {},
    }
};

/**
 * A reducer takes two arguments, the current state and an action.
 */
export default function (state, action) {
    state = state || initialState;
    let newState;

    console.log('crudReducer');
    console.log(action);
    console.log(state); 

    switch (action.type) {
        case ENTITY_CREATE:
            return Object.assign({}, state, action.data);

        case ENTITY_UPDATE:
            return Object.assign({}, state, action.data);

        case ENTITY_FETCH:
            return Object.assign({}, state, action.data);

        case GET_ENTITY_LIST:
            return Object.assign({}, state, {timeItems:action.data.data});

        case ENTITY_DELETE:
            const data = Object.assign({}, state);
            return data.filter(data => data.id !== action.data.id);

        case SELECT_ENTITY_ITEM:
            return Object.assign({}, state, action.data);

        case CLEAR_ENTITY_LIST:
            return {};

        default:            
            return state;
    }
}