// Import actionType constants
import {
    LOG_IN_SUCCESS,
    LOG_IN_FAILURE,
    LOG_OUT_SUCCESS,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAILURE,
    REMOVE_SIGN_UP_ERROR,
    REMOVE_LOGIN_ERROR,
} from '../constants/actionType';

// Login

export function loginSuccess(data) {
    return {
        type: LOG_IN_SUCCESS,
        data
    }
}

export function loginFailure(error) {
    return {
        type: LOG_IN_FAILURE,
        error
    }
}

export function removeLoginError() {
    return {
        type: REMOVE_LOGIN_ERROR
    }
}

export function logoutSuccess() {
    return {
        type: LOG_OUT_SUCCESS
    }
}

// Register

export function signupSuccess(data) {
    return {
        type: SIGN_UP_SUCCESS,
        data
    }
}

export function signUpFailure(error) {
    return {
        type: SIGN_UP_FAILURE,
        error
    }
}

export function removeSignUpError() {
    return {
        type: REMOVE_SIGN_UP_ERROR
    }
}
