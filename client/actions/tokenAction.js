import {getUserData} from '../utils/storageUtil';
import * as AuthAction from './authAction';

export function verifyToken() {
    return (dispatch) => {
        const userdata = getUserData();
        if (userdata.token) {
            dispatch(AuthAction.loginSuccess(userdata));
        }
    };
}