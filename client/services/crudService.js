import history from '../utils/history';
import * as AuthAction from '../actions/authAction'; 
import {setUserData} from '../utils/storageUtil';
import {BASE_URL} from '../config/config';


/**
 * Import all commonAction as an object.
 */
import * as commonAction from '../actions/commonAction';

/**
 * Import all httpService as an object.
 */
import * as httpService from './httpService';


/**
 * CRUD actions for the application.
 * Every time an action that requires the API is called, it first dispatch an "apiRequest" action.
 *
 * entity = 'Product', 'Employee', ...
 */ 


export function fetchAll(entity) { 
    console.log('fetchAll crudaction');
    console.log(entity);
    return function (dispatch) {
        return httpService.fetchEntity(entity).then((response) => {
            console.log(response);
            dispatch(commonAction.list(entity, response.data));
        })
            .catch((error) => {
                dispatch(commonAction.failure(error));
            });
    };
}

export function fetchById(entity, id) {
    return function (dispatch) {
        return httpService.fetchEntityById(entity, id).then((response) => {
            dispatch(commonAction.selectItem(entity, response.data));
        })
            .catch((error) => {
                dispatch(commonAction.failure(error));
            });
    };
}

export function storeItem(entity, data) {
    return function (dispatch) {
        console.log('storeItem');
        return httpService.storeEntity(entity, data).then((response) => {

            console.log('response');
            console.log(response);
            console.log(entity);
            console.log(data);
            console.log(dispatch);

            if (entity == 'users') {

                if (response && response.data && response.data.token) {

                    dispatch(AuthAction.loginSuccess({
                        token: response.data.token, 
                        email: response.data.email, 
                        fname: response.data.fname, 
                        lname: response.data.lname
                        }
                    ));

                    setUserData(response.data.token, response.data.email, response.data.fname, response.data.lname);

                    window.location.href = BASE_URL + 'dashboard'; 
                } else {
                    
                }  

            }

            //history.goBack();
        })
        .catch((error) => {
            console.log('catch');
            dispatch(commonAction.failure(error));
        });
    };
}

export function updateItem(entity, data, id) {
    return function (dispatch) {
        return httpService.updateEntity(entity, data, id).then((response) => {
            //history.goBack();
        })
            .catch((error) => {
                dispatch(commonAction.failure(error));
            });
    };
}

export function destroyItem(entity, id, data) {
    return function (dispatch) {
        return httpService.destroyEntity(entity, id).then((response) => {
            dispatch(fetchAll(entity, data));
        })
            .catch((error) => {
                dispatch(commonAction.failure(error));
            });
    };
}

export function submitForm(entity, data, id) {
    console.log('submitForm');
    return function (dispatch) {
        if (id) {
            console.log('submitForm with id');
            dispatch(updateItem(entity, data, id));
        } else {
            console.log('submitForm no id');
            console.log(entity);
            console.log(data);
            //storeItem(entity, data);
            dispatch(storeItem(entity, data));
        }
    }
} 