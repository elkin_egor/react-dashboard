import axios from 'axios';

// Import custom actionType
import * as authAction from '../actions/authAction'; 

import {BASE_URL, API_URL} from '../config/config';
import {setUserData, clearToken} from '../utils/storageUtil'; 

import * as httpService from './httpService';

export function login({email, password}) {

    return function (dispatch) {
        axios.post(API_URL + 'auth/login', {email, password}).then((response) => {

            console.log('login response');
            console.log(response);

            dispatch(authAction.loginSuccess({
                token: response.data.token, 
                email: response.data.email, 
                fname: response.data.fname, 
                lname: response.data.lname
                }
            ));

            setUserData(response.data.token, response.data.email, response.data.fname, response.data.lname);

            window.location.href = BASE_URL + 'dashboard';
        })
            .catch((error) => {
                dispatch(authAction.loginFailure(error));
            });
    };
}

export function logout() {
    return function (dispatch) {

        clearToken();

        dispatch(authAction.logoutSuccess());

        window.location.href = BASE_URL;
        return false;
    };
}

export function signup(data) {
    return function (dispatch) {
        console.log('storeItem');
        return httpService.storeEntity('users', data).then((response) => {

            console.log('response');
            console.log(response);

            if (response.data && response.data.token) {

                console.log('SignUp resp ok');

                dispatch(authAction.loginSuccess({
                    token: response.data.token, 
                    email: response.data.email, 
                    fname: response.data.fname, 
                    lname: response.data.lname
                    }
                ));

                setUserData(response.data.token, response.data.email, response.data.fname, response.data.lname);

                window.location.href = BASE_URL + 'dashboard'; 
            } else {

                console.log('SignUp resp Failure');
                dispatch(authAction.signUpFailure('token is missing in response'));
            }  


            //history.goBack();
        })
        .catch((error) => {
            console.log('catch');
            dispatch(authAction.signUpFailure(error));
        });
    };
}