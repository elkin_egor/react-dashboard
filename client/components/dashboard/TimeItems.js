import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {cyan} from '@material-ui/core/colors';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import TimeFormContainer from '../../containers/dashboard/TimeFormContainer'; 

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

const TimeItems = props => {

    const {classes} = props;

    console.log('TimeItems');
    console.log(props);

    return (
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Time spent</TableCell>
                <TableCell align="right">Description</TableCell>
                <TableCell align="right">Category</TableCell>
                <TableCell align="right">Project</TableCell>
                <TableCell align="right">User</TableCell>
                <TableCell align="right">Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.data.map(n => {
                return (
                  <TableRow key={n.id}>
                    <TableCell component="th" scope="row">{n.time_spent}</TableCell>
                    <TableCell align="right">{n.description}</TableCell>
                    <TableCell align="right">{n.category_id}</TableCell>
                    <TableCell align="right">{n.project_id}</TableCell>
                    <TableCell align="right">{n.user_id}</TableCell>
                    <TableCell align="right">{n.work_date}</TableCell>
                  </TableRow>
                );
              })}
              <TableRow key={0}>
                <TableCell colSpan="6">
                  <TimeFormContainer/>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
    )
};

TimeItems.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array,
};

export default withStyles(styles)(TimeItems)