import React, {Component} from 'react'; 
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

import TimeItems from './TimeItems'; 


const Dashboard = props => {

    const {loading, timeItems, getProducts} = props;

    console.log('Dashboard');

    return (
        <div>
            <h2 style={{paddingBottom: '15px'}}>Logged time</h2>

            <Grid container spacing={24}>
               <Grid item xs>
                    {(loading === true) ?
                        <p>Loading timeItems...</p> :
                        [(timeItems.length === 0) ?
                            <p>No items</p> :
                            <TimeItems key={1} data={timeItems}/>
                        ]
                    }
                    
                </Grid>
            </Grid>

        </div>
    )
};

Dashboard.propTypes = {
    getProducts: PropTypes.func.isRequired,
    timeItems: PropTypes.array,
};

Dashboard.defaultProps = {
    getProducts: f=>f,
    timeItems: [],
} 

export default Dashboard