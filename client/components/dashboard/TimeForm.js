import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
import {withStyles} from '@material-ui/core/styles';
import {Card, CardHeader, CardContent} from '@material-ui/core';
import Button from '@material-ui/core/Button';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import Grid from '@material-ui/core/Grid';

// Import custom components
import renderText from '../common/form/renderText';

const styles = {
    btnDiv: {
        textAlign: 'center'
    },
    btn: {
        marginTop: 21,
    },
    col: {
      margin: 0,
    }
};

const TimeForm = props => {

    const {handleSubmit, onSubmit, classes} = props;

    return (
        <form method="post" onSubmit={handleSubmit(onSubmit)}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="flex-start"
              spacing={16}
            >
              <Grid item xs={2}>
                <Field
                    type="date"
                    name="item_date"
                    component={renderText}
                    label="Date"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <Field
                    type="text"
                    name="time_spent"
                    component={renderText}
                    label="Time spent"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <Field
                    type="text"
                    name="description"
                    component={renderText}
                    label="Description"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <Field
                    type="text"
                    name="category_id"
                    component={renderText}
                    label="category_id"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <Field
                    type="text"
                    name="project_id"
                    component={renderText}
                    label="project_id"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <Field
                    type="text"
                    name="user_id"
                    component={renderText}
                    label="user_id"
                    className={classes.col}
                />
              </Grid>
              <Grid item xs={2}>
                <div className={classes.btnDiv}>
                    <Button className={classes.btn} type="submit" variant="raised" color="primary">Add</Button>
                </div>
              </Grid>
            </Grid>
        </form>
    )
};

const validateTimeItem = values => {
    const errors = {};

    const requiredFields = [
        'time_spent',
        'description',
        'category_id',
        'project_id',
        'user_id'
    ];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = '(The ' + field + ' field is required.)';
        }
    });

    return errors
};

TimeForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired
};

export default reduxForm({
    form: 'TimeForm',
    validate: validateTimeItem
})(withRouter(withStyles(styles)(TimeForm)))