import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
//import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import SettingsIcon from '@material-ui/icons/Settings';
import HelpIcon from '@material-ui/icons/Help';
import NotificationsIcon from '@material-ui/icons/Notifications';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';

//const drawerWidth = 250; 
/*
const styles = theme => ({
    drawerPaper: {
        z-index: 9,
        position: 'relative',
        height: 'auto',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        width: 60,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    drawerInner: {
        // Make the items inside not wrap when transitioning:
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        height: 56,
        [theme.breakpoints.up('sm')]: {
            height: 64,
        },
    },
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        width: 60,
        height: 60,
    },
});
*/
const MiniDrawer = (props) => {

    const navDrawerOpen = props.navDrawerOpen;
    const classes = props.classes;
    const user = props.user;
    const handleToggleDrawer = props.handleToggleDrawer;

    return (
        <Drawer
            variant="permanent"
            classes={{
                paper: classNames(classes.drawerPaper, !navDrawerOpen && classes.drawerPaperClose),
            }}
            open={navDrawerOpen}
        >
            <div className={classes.toolbarIcon}>
                <IconButton onClick={handleToggleDrawer}>
                  <ChevronLeftIcon />
                </IconButton>
            </div>    
            <div className={classNames(classes.toolbarUser, !navDrawerOpen && classes.hiddenEl)}>
                <Avatar alt="User" src="/img/avatar5.png" className={classNames(classes.avatar, !navDrawerOpen && classes.minAvatar)}/>
                <Typography component="p" className={classes.avatarText}>
                    {user.fname} {user.lname}
                </Typography>
                <Typography component="span" className={classes.avatarText}>
                    {user.email}
                </Typography>
            </div>
            <Divider />
            <div className="box" className={classNames(classes.drawerMenuTitle, !navDrawerOpen && classes.hiddenEl)}>
                MAIN NAVIGATION
            </div>

            <List>
                <ListItem button>
                    <ListItemIcon>
                        <HomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Dashboard"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary="Users"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <LocalTaxiIcon />
                    </ListItemIcon>
                    <ListItemText primary="Products"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <NotificationsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Notifications"/>
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <SettingsIcon />
                    </ListItemIcon>
                    <ListItemText primary="Settings"/>
                </ListItem>
                <Divider/>
                <ListItem button>
                    <ListItemIcon>
                        <HelpIcon />
                    </ListItemIcon>
                    <ListItemText primary="Help"/>
                </ListItem>
            </List>
        </Drawer>
    )
};

MiniDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    navDrawerOpen: PropTypes.bool
};

//export default withStyles(styles)(MiniDrawer)
export default MiniDrawer