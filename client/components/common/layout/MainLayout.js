import React, {Component} from 'react';
import {connect} from 'react-redux'; 
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

// Import custom components 
import Header from '../header/Header';
import MiniDrawer from '../drawer/MiniDrawer';
import Footer from '../footer/Footer';

const drawerWidth = 250;
const styles = theme => ({
    /* main layout */
    root: { // Root div
        width: '100%',
        height: '100%',
        display:'flex',
        zIndex: 1,
        overflow: 'hidden',
    },
    appFrame: { // Divide footer and content above
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    content: { // Content div
        width: '100%',
        flexGrow: 1,
        padding: 24,
        height: 'calc(100% - 56px)',
        marginTop: 56,
        [theme.breakpoints.up('sm')]: {
            height: 'calc(100% - 64px)',
            marginTop: 64,
        },
    },
    /* Header layout */
    appBar: { // Header styles
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: { // Header styles when sidebar is open
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: { // Header open - close icon
        marginLeft: -15,
    },
    menuButtonHidden: { // Header open - close icon when sidebar is open
        display: 'none',
    },
    hiddenEl: { // Header open - close icon when sidebar is open
        display: 'none',
    },
    flex: { // Header title style
        flex: 1
    },
    /* Sidebar layout */
    drawerPaper: { // Sidebar style
        position: 'relative',
        height: '100%',
        overflow: 'hidden',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: { // Sidebar style when it's close
        width: '60px',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    toolbar: { // just Mixin
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: { // Open-close sidebar icon
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    toolbarUser: { // Open-close sidebar icon
        width: drawerWidth,
    },
    drawerMenuTitle:{ // Sidebar menu title
        backgroundColor: '#eee', 
        padding: '8px 16px',
        width: drawerWidth,
    },
    avatar: { // User image style
        margin: 10,
        width: '60px',
        height: '60px',
    }, 
    minAvatar: { // User image style
        width: '40px',
        height: '40px',
    },
    avatarText: { // User image style
        margin: 10,
    },
    /* Footer styles */
    legal: { 
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: 0,
        width: '97.6%',
        borderTop: '1px solid #eee',
        padding: '15px',
        overflow: 'hidden',
        textAlign: 'center'
    }
});

class MainLayoutComp extends Component {

    constructor(props) {
        super(props);
        this.state = {open: true};
    }

    handleToggle = () => {

        console.log('handleToggle');

        return this.setState({open: !this.state.open});

        console.log(this);

    }

    render() {
        let {open} = this.state;
        const classes = this.props.classes;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <Header classes={classes} navDrawerOpen={open} handleToggleDrawer={this.handleToggle}/>
                    <MiniDrawer classes={classes} navDrawerOpen={open} user={this.props.user}  handleToggleDrawer={this.handleToggle}/>
                    <main className={classes.content}>
                        {this.props.children}
                    </main>
                </div>

                <Footer  classes={classes}  />

            </div>
        )
    }

}

MainLayoutComp.propTypes = {
    classes: PropTypes.object.isRequired,
    children: PropTypes.element
};

/**
 * Map the state to props.
 */
const mapStateToProps = function (storestate) {
    console.log('mainlayout cont mapStateToProps');
    return { 
        user: {
            email: storestate.auth.email,
            fname: storestate.auth.fname,
            lname: storestate.auth.lname
        }
    }
}

const MainLayout = connect(
    mapStateToProps
)(MainLayoutComp)

export default withStyles(styles)(MainLayout)