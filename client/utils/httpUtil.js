import axios from 'axios';
import {getToken} from './storageUtil'

export function fetch(url, endpoint) {

    console.log('httputils fetch');

    let res = axios
        .get(url + endpoint, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer' + ' ' + getToken()
            }
        });

    console.log(res);

    return res;
}

export function store(url, endpoint, data) {

    console.log('httputils store');

    let res =  axios
        .post(url + endpoint, data, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer' + ' ' + getToken()
            }
        });

    console.log('httputils store res');
    console.log(res);

    return res;
}

export function update(url, endpoint, data) {

    console.log('httputils update');

    let res =  axios
        .put(url + endpoint, data, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer' + ' ' + getToken()
            }
        });

    console.log(res);

    return res;
}

export function destroy(url, endpoint) {

    console.log('httputils destroy');

    let res =  axios
        .delete(url + endpoint, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer' + ' ' + getToken()
            }
        });

    console.log(res);

    return res;
}