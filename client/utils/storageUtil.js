//export const setToken = token => localStorage.setItem('token', token);

export const setUserData = (token, email, fname, lname) => {
	localStorage.setItem('fname', fname)
	localStorage.setItem('lname', lname)
	localStorage.setItem('email', email)
	localStorage.setItem('token', token)
}; 

export const getUserData = () => {
	return {
		'fname' : localStorage.getItem('fname'),
		'lname' : localStorage.getItem('lname'),
		'email' : localStorage.getItem('email'),
		'token' : localStorage.getItem('token'),
	}
}; 

export const getToken = () => localStorage.getItem('token');
export const clearToken = () => localStorage.removeItem('token');