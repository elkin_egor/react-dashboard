import React, {Component} from 'react'; 
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Grid from '@material-ui/core/Grid';

import {TIME_ITEMS} from '../../constants/entity'
import * as crudService from '../../services/crudService'

// Import custom components
import TimeForm from '../../components/dashboard/TimeForm';

class TimeFormContainer extends Component {

    constructor(props) {
        
        super(props);        
        this.submitForm = this.submitForm.bind(this);
    }

    /**
     * Submit the form.
     *
     * @param {object} formProps
     */
    submitForm(formProps) {

        console.log(formProps);
        this.props.actions.submitForm(TIME_ITEMS, formProps);
    }

    render() {

        return (
            <TimeForm
                onSubmit={this.submitForm}
            />
        );
    }

}

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, crudService), dispatch)
});

export default connect(null, mapDispatchToProps)(TimeFormContainer)