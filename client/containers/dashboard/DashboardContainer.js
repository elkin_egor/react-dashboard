import React, {Component} from 'react';
import {connect} from 'react-redux'; 
import {TIME_ITEMS} from '../../constants/entity'
import {fetchAll} from '../../services/crudService'
import Dashboard from '../../components/dashboard/Dashboard';

class DashboardCont extends Component {

    constructor(props) { 

        super(props);

        this.state = {
        	loading: true
        };

        this.getTimeItems = this.getTimeItems.bind(this);
    }

    getTimeItems() {
        this.props.getTimeItems();
    }

    componentWillMount() {
    	console.log('componentWillMount')  
    }

    componentWillUpdate() {
        console.log('componentWillUpdate')
        this.state.loading = false;
    }

    componentDidMount() {
    	console.log('componentDidMount')
	    this.getTimeItems();
	}

    render() {
        return (
            <Dashboard getTimeItems={this.getTimeItems} timeItems={this.props.timeItems} loading={this.state.loading} />
        )
    }
}

/**
 * Map the state to props.
 */
const mapStateToProps = function (storestate) {
    console.log('mapStateToProps');
    return { timeItems: storestate.crud.timeItems }
}

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
    getTimeItems() {
        dispatch(fetchAll(TIME_ITEMS))
    },
});

const DashboardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardCont)

export default DashboardContainer