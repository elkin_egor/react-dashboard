import React, {Component} from 'react'; 
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as authService from '../../services/authService';
import {removeLoginError} from '../../actions/authAction';

// Import custom components
import LoginForm from '../../components/auth/LoginForm';

class LoginContainer extends Component {

    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
        this.removeLoginError = this.removeLoginError.bind(this);
    }

    removeloginError() {
        this.props.removeLoginError();
    }

    /**
     * Submit the form.
     *
     * @param {object} formProps
     */
    submitForm(formProps) {
        this.props.actions.login(formProps);
    }

    render() {
        if (this.props.loginError == true) {
            console.log('qqq set timeout');
            setTimeout(() => {
              this.removeloginError();
            }, 3000)
        }
        return (
            <LoginForm
                onSubmit={this.submitForm} loginError={this.props.loginError}
            />
        )
    }

}

/**
 * Map the state to props.
 */
const mapStateToProps = state => ({
    token: state.auth.token,
    isAuthenticated: state.auth.isAuthenticated,
    loginError: state.auth.loginError
});

/**
 * Map the actions to props.
 */
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Object.assign({}, authService), dispatch),
    removeloginError() {
        dispatch(removeloginError())
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)